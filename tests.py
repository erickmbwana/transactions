import unittest

from core import rate_customers, _get_grouped_customers, _consecutive_groups


class CustomerRatingTestCase(unittest.TestCase):

    def test_consecutive_groups(self):
        iterable = [736331, 736332, 736336, 736339, 736340, 736341, 736343]
        actual = [list(g) for g in _consecutive_groups(iterable)]
        expected = [[736331, 736332], [736336], [736339, 736340, 736341], [736343]]
        self.assertListEqual(actual, expected)

    def test_customer_grouping(self):
        expected = [
            ('ACC1', [736331, 736332, 736336, 736339, 736340, 736341, 736343]),
            ('ACC2', [736331, 736332, 736333, 736334, 736335, 736337, 736342, 736343])
        ]

        actual = _get_grouped_customers('transaction_data_1.csv')
        self.assertListEqual(actual, expected)

    def test_customer_rating(self):
        rated = rate_customers('transaction_data_2.csv', 2)
        self.assertListEqual(rated, ['ACC1', 'ACC4'])


if __name__ == '__main__':
    unittest.main()
