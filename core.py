import argparse
import csv
import datetime
from itertools import groupby
from operator import itemgetter


def _consecutive_groups(iterable, ordering=lambda x: x):
    """
    Create groups of consecutive items from an iterable.

    Taken with much pleasure from the more-itertools python package. Many thanks!
    https://github.com/erikrose/more-itertools/blob/master/more_itertools/more.py#L1678-L1712
    """
    for k, g in groupby(
        enumerate(iterable), key=lambda x: x[0] - ordering(x[1])
    ):
        yield map(itemgetter(1), g)


def _get_grouped_customers(transactions_csv_file):
    """
    Creates and returns a suitable customers' data structure from a CSV file.

    Associate a customer with all the dates(date ordinals) they made payments.
    """
    customers = {}
    with open(transactions_csv_file, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            group = customers.setdefault(row['customer_id'], [])
            day = datetime.datetime.strptime(
                (row['transaction_date']),
                '%Y-%m-%d %H:%M:%S'
            )
            group.append(day.toordinal())
    customers = [(k, sorted(v)) for k, v in customers.items()]
    return customers


def rate_customers(transactions_csv_file, n):
    """
    Rate customers based on the longest periods of consecutive daily payments.

    Returns a list of the best n customer_ids rated
    based on the longest periods of consecutive daily payments.
    """
    customers = _get_grouped_customers(transactions_csv_file)

    # Create sub lists of consecutive groups for each customer
    rated_customers = {}
    for idx, date_ordinals in customers:
        for g in _consecutive_groups(set(date_ordinals)):
            date_group = rated_customers.setdefault(idx, [])
            date_group.append(sum(1 for _ in list(g)))

    # Get the max for each customer, i.e the longest consecutive payments period
    final = [(k, max(v)) for k, v in rated_customers.items()]

    # sort by longest run, then by customer_id
    final = sorted(final, key=lambda x: (-x[1], x[0]))

    return([customer_id for customer_id, _ in final[:n]])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "transactions_csv_file",
        help="path to file with transactions data"
    )
    parser.add_argument(
        "n",
        type=int,
        help="number of best customer to display in a list"
    )
    args = parser.parse_args()
    print(rate_customers(args.transactions_csv_file, args.n))
