### RATING CUSTOMERS

This repository contains a simple Python script(`core.py`) rating customers
based on their longest runs of consecutive daily payments.

The bulk of the work happens in the `core.rate_customers` function which is the
function specifically designed in answer to the coding task.

### Usage
Clone the repo:
```
git clone https://gitlab.com/erickmbwana/transactions.git

```
Make sure you're within the repository and using Python 2.7 or Python >=3.5. You can run :

```
# Some CSV files are in the repository for testing purposes.
python core.py transaction_data_3.csv 3

```
For help run: `python core.py -h`

The scripts takes two mandatory arguments which are passed to the `core.rate_customers` function:
- `transactions_csv_file` - a string specifying the path to the CSV file with the transactions data and
- `n` - an integer specifying the number of best customers to display in an array.


### Tests
You can run the tests thus:

```
python tests.py
```
